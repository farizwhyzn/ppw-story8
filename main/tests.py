from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from .views import home, query
from selenium import webdriver

class Test(TestCase):
    def test_home_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_views(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_use_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_query_is_exist(self):
        response = Client().get(f"{reverse('main:query')}?query=book")
        self.assertEqual(response.status_code, 200)