from django.shortcuts import render
from django.http import JsonResponse
from requests import get
import json

def home(request):
    return render(request, 'main/home.html')


def query(request):
    keyword = request.GET['query']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + keyword
    info = get(url).content
    info = json.loads(info)
    data = []
    for item in info['items']:
        peeps = {}
        try:
            peeps['title'] = item['volumeInfo']['title']
        except KeyError:
            peeps['title'] = ''
        try:
            peeps['author'] = item['volumeInfo']['authors']
        except KeyError:
            peeps['author'] = ['unknown']
        try:
            peeps['publisher'] = item['volumeInfo']['publisher']
        except KeyError:
            peeps['publisher'] = 'unknown'
        try:
            peeps['thumbnail'] = item['volumeInfo']['imageLinks']['smallThumbnail']
        except KeyError:
            peeps['thumbnail'] = f'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjvBjFogjHCuwfY71U6JfLxsl7ZLV00OD7KQ&usqp=CAU'
        data.append(peeps)
    return JsonResponse(data, safe=False)