from django.test import TestCase
from django.urls import resolve, reverse

from .views import index, signup, home
from django.contrib.auth.views import LoginView, LogoutView

from django.contrib.auth.models import User

class UnitTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('Auth:index'))
        self.assertEqual(found.func, index)

    def test_index_page_returns_correct_html(self):
        response = self.client.get(reverse('Auth:index'))
        html = response.content.decode('utf8')
        self.assertIn('Story 9', html)
        self.assertTemplateUsed(response, 'halo.html')

    def test_login_page_returns_correct_html(self):
        response = self.client.get(reverse('Auth:login'))
        html = response.content.decode('utf8')
        self.assertIn('In', html)
        self.assertTemplateUsed(response, 'login.html')

    def test_logout_page_returns_correct_html(self):
        response = self.client.get(reverse('Auth:logout'))
        html = response.content.decode('utf8')
        self.assertIn('Logged out!', html)
        self.assertTemplateUsed(response, 'logout.html')

    def test_signup_url_resolves_to_signup_view(self):
        found = resolve(reverse('Auth:signup'))
        self.assertEqual(found.func, signup)

    def test_signup_page_returns_correct_html(self):
        response = self.client.get(reverse('Auth:signup'))
        html = response.content.decode('utf8')
        self.assertIn('Sign Up', html)
        self.assertTemplateUsed(response, 'signup.html')

    def test_user_url_resolves_to_user_view(self):
        found = resolve(reverse('Auth:nama'))
        self.assertEqual(found.func, home)



