from django.urls import path
from django.contrib.auth import views as authLib
from . import views

app_name = 'Auth'

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='nama'),
    path('signup/', views.signup, name='signup'),
    path('login/', authLib.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', authLib.LogoutView.as_view(template_name='logout.html', next_page=None), name='logout'),
]
